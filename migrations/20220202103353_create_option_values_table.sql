-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table option_values
(
    id              bigserial
        constraint option_values_pk primary key,
    option_id       bigint default null  not null,
    slug            text                 not null,
    value           text   default null,
    is_active       bool   default false not null,
    is_linked       bool   default false not null,
    is_hide_sitemap bool   default false not null,
    sort            int    default 0     not null,

    created_at      timestamptz          not null,
    updated_at      timestamptz          not null,

    foreign key (option_id) references options (id) on delete cascade on update cascade
);

create unique index option_values_option_id_slug_uidx
    on option_values (option_id, slug);


-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table option_values;
