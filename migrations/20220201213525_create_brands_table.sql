-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table brands
(
    id              bigserial
        constraint brands_pk primary key,
    title           text               not null,
    slug            text               not null,
    is_active       bool default false not null,
    is_hide_sitemap bool default false not null,

    created_at      timestamptz        not null,
    updated_at      timestamptz        not null
);

create unique index brands_slug_uidx
    on brands (slug);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table brands;
