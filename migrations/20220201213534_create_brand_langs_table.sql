-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table brand_langs
(
    id                 bigserial
        constraint brand_langs_pk primary key,
    lang_id            int         not null,
    parent_id          bigint      not null,
    description        text,

    seo_h1             text        not null,
    seo_title          text,
    seo_description    text,
    seo_og_title       text,
    seo_og_description text,

    created_at         timestamptz not null,
    updated_at         timestamptz not null,

    foreign key (parent_id) references brands (id) on delete cascade on update cascade,
    foreign key (lang_id) references languages (id) on delete cascade on update cascade
);
create unique index brand_langs_parent_id_lang_id_uidx
    on brand_langs (parent_id, lang_id);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table brand_langs;
