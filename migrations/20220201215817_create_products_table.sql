-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table products
(
    id              bigserial
        constraint products_pk primary key,
    category_id     bigint              not null,
    brand_id        bigint              not null,
    slug            text                not null,
    sku             text                not null,
    price           float default 0     not null,
    is_active       bool  default false not null,
    is_hide_sitemap bool  default false not null,

    created_at      timestamptz         not null,
    updated_at      timestamptz         not null,

    foreign key (category_id) references categories (id) on delete set null on update set null,
    foreign key (brand_id) references brands (id) on delete set null on update set null
);

create unique index products_sku_uidx
    on products (sku);
create unique index products_slug_uidx
    on products (slug);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table products;
