-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table if not exists languages
(
    id         serial
        constraint languages_pk primary key,
    title      text        not null,
    slug       text        not null,
    code       text        not null,
    is_main    bool default false,
    is_active  bool default false,

    created_at timestamptz not null,
    updated_at timestamptz not null
);

create unique index languages_slug_uidx
    on languages (slug);

create unique index languages_code_uidx
    on languages (code);

insert into languages (title, slug, code, is_main, is_active, created_at, updated_at)
values ('Português', 'pt', 'pt-BR', true, true, now(), now()),
       ('English', 'en', 'en-US', false, true, now(), now());

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table languages;
