-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table option_langs
(
    id                 bigserial
        constraint option_langs_pk primary key,
    lang_id            int         not null,
    parent_id          bigint      not null,
    title              text        not null,
    description        text,

    created_at         timestamptz not null,
    updated_at         timestamptz not null,

    foreign key (parent_id) references options (id) on delete cascade on update cascade,
    foreign key (lang_id) references languages (id) on delete cascade on update cascade
);

create unique index option_langs_parent_id_lang_id_uidx
    on option_langs (parent_id, lang_id);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table option_langs;
