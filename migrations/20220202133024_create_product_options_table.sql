-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table product_options
(
    id         bigserial
        constraint product_options_pk primary key,
    product_id bigint default null not null,
    option_id  bigint default null not null,
    value_id   bigint default null not null,

    created_at timestamptz         not null,
    updated_at timestamptz         not null,

    foreign key (product_id) references products (id) on delete cascade on update cascade,
    foreign key (option_id) references options (id) on delete cascade on update cascade,
    foreign key (value_id) references option_values (id) on delete cascade on update cascade
);

create unique index product_options_parent_id_option_id_value_id_uidx
    on product_options (product_id, option_id, value_id);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table product_options;
