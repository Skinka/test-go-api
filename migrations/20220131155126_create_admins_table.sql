-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table if not exists admins
(
    id                serial
        constraint admins_pk primary key,
    name              text,
    email             text        not null,
    password          text        not null,
    remember_token    text,
    is_active         bool default false,
    email_verified_at timestamptz,

    created_at        timestamptz not null,
    updated_at        timestamptz not null
);

create unique index admins_email_uidx
    on admins (email);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table admins;
