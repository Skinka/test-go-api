-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table options
(
    id          bigserial
        constraint options_pk primary key,
    category_id bigint default null,
    is_active   bool   default false not null,
    is_filtered bool   default false not null,
    slug        text                 not null,
    sort        int    default 0     not null,

    created_at  timestamptz          not null,
    updated_at  timestamptz          not null,

    foreign key (category_id) references categories (id) on delete set null on update set null
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table options;
