-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table categories
(
    id              bigserial
        constraint categories_pk primary key,
    slug            text                 not null,
    is_active       bool   default false not null,
    is_hide_sitemap bool   default false not null,
    sort            int    default 0     not null,

    created_at      timestamptz          not null,
    updated_at      timestamptz          not null
);

create unique index categories_slug_uidx
    on categories (slug);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table categories;
