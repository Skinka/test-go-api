-- +goose Up
-- SQL in this section is executed when the migration is applied.
create table option_value_langs
(
    id                 bigserial
        constraint option_value_langs_pk primary key,
    lang_id            int         not null,
    parent_id          bigint      not null,
    value              text default null,

    seo_h1             text        not null,
    seo_title          text,
    seo_description    text,
    seo_og_title       text,
    seo_og_description text,

    created_at         timestamptz not null,
    updated_at         timestamptz not null,

    foreign key (parent_id) references option_values (id) on delete cascade on update cascade,
    foreign key (lang_id) references languages (id) on delete cascade on update cascade
);

create unique index option_value_langs_parent_id_lang_id_uidx
    on option_value_langs (parent_id, lang_id);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
drop table option_value_langs;
