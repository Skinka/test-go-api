# API

## Config

Make api.json

## Build

```
go build -ldflags "-X main.version=1" -o api_service ./cmd/api/
```

## Make migration

```
goose -dir="./migrations" create name_migration sql
```

## Linters

```
golangci-lint run -c ./.golangci.yml

golangci-lint run --fix -c ./.golangci.yml
```
