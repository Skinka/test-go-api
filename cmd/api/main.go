package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"

	"gitlab.com/mi-max/api/internal/database"
	"gitlab.com/mi-max/api/internal/handler"
	"gitlab.com/mi-max/api/internal/web_server"
	"gitlab.com/mi-max/api/pkg/config"
	"gitlab.com/mi-max/api/pkg/logger"
)

var version = "unknown"

func main() {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	conf, err := config.LoadAPIConfig()
	if err != nil {
		if errors.Is(err, flag.ErrHelp) {
			return
		}
		fmt.Printf("failed to load config: %s\n", err)
		os.Exit(1)
	}

	log, logSync := logger.New(&conf.Log)
	defer logSync()

	log = log.With(zap.String("version", version))
	log.Debug("start application")

	db, err := database.New(&conf.DB, &conf.Log, log)
	if err != nil {
		log.With(zap.Error(err)).Fatal("make database connect")
	}

	if err = db.RunMigrations(); err != nil {
		log.With(zap.Error(err)).Fatal("run migrate db")
	}

	hs := handler.New(log, db)
	if err = hs.Init(); err != nil {
		log.With(zap.Error(err)).Fatal("handler init")
	}

	ws := web_server.New(log, &conf.WebServer, hs, db)
	go ws.Run()

	<-quit

	log.Info("server exited properly")
}
