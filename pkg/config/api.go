package config

import (
	"flag"
	"fmt"

	"github.com/cristalhq/aconfig"
)

// WebServer config.
type WebServer struct {
	Timeout uint   `default:"60" usage:"Server timeout"`
	Port    string `default:"3004" usage:"Port for API"`
}

// APIConfig main config.
type APIConfig struct {
	Help      bool `default:"false" usage:"Prints help message with keys"`
	DB        DB
	Log       Log
	WebServer WebServer
}

// LoadAPIConfig loading config.
func LoadAPIConfig() (*APIConfig, error) {
	conf := &APIConfig{}

	loader := aconfig.LoaderFor(conf, aconfig.Config{
		SkipFlags: true,
		EnvPrefix: "",
		Files:     []string{"api.json"},
	})

	if err := loader.Load(); err != nil {
		return nil, fmt.Errorf("loader.Load: %w", err)
	}

	if conf.Help {
		loader.Flags().PrintDefaults()

		return nil, flag.ErrHelp
	}

	return conf, nil
}
