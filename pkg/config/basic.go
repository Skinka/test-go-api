// Package config application settings.
package config

// Log конфигурация логирования сервиса.
type Log struct {
	Debug   bool `default:"true" usage:"Enables printing of debug messages"`
	DebugDB bool `default:"true" usage:"Enables printing of debug messages"`
}

// DB конфигурация подключения и начальной инициализации базы данных.
type DB struct {
	Host string `default:"localhost" usage:"Host of the DB connect"`
	Port string `default:"5432" usage:"Port of the DB connect"`
	Name string `default:"database" usage:"Name of the DB"`
	User string `default:"postgres" usage:"Username of the connect"`
	Pass string `default:"" usage:"Password of the connect"`

	MaxOpenConns int `default:"20" usage:"Max count of open connect in pool of DB"`
	MaxIdleConns int `default:"10" usage:"Max count of idle connect in pool of DB"`

	MigrationPath string `default:"migrations" usage:"migrations path"`
}
