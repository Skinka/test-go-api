// Package middleware language middleware.
package middleware

import (
	"context"
	"net/http"
)

// LanguageKey key for get lang from request context.
type LanguageKey struct{}

// Language middleware set zap logger.
func Language(languages []string, defaultLang string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			lang := r.URL.Query()["lang"]

			switch {
			case len(lang) > 0:
				if !hasLanguage(lang[0], languages) {
					lang[0] = defaultLang
				}
			default:
				lang = append(lang, defaultLang)
			}

			ctx = context.WithValue(ctx, LanguageKey{}, lang[0])
			next.ServeHTTP(w, r.WithContext(ctx))
		}

		return http.HandlerFunc(fn)
	}
}

func hasLanguage(lang string, languages []string) bool {
	for i := 0; i < len(languages); i++ {
		if languages[i] == lang {
			return true
		}
	}

	return false
}
