package logger_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/mi-max/api/pkg/config"
	"gitlab.com/mi-max/api/pkg/logger"
)

func TestLogger(t *testing.T) {
	t.Parallel()

	l, logSync := logger.New(&config.Log{Debug: true, DebugDB: true})
	defer logSync()

	assert.NotNil(t, l)
	assert.IsType(t, func() {}, logSync)
}
