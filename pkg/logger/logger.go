// Package logger include methods for do with logs.
package logger

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/mi-max/api/pkg/config"
)

// New make Logger.
func New(conf *config.Log) (*zap.Logger, func()) {
	encoderConf := zapcore.EncoderConfig{
		MessageKey:     "msg",
		LevelKey:       "level",
		TimeKey:        "@timestamp",
		NameKey:        "logger",
		CallerKey:      "caller",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.StringDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}

	logLevel := zapcore.InfoLevel
	if conf.Debug {
		logLevel = zapcore.DebugLevel
	}

	core := zapcore.NewCore(zapcore.NewJSONEncoder(encoderConf), zapcore.Lock(os.Stderr), logLevel)

	log := zap.New(core)

	return log, func() {
		log.Info("syncing logger")

		if err := log.Sync(); err != nil {
			log.Error("logger.Sync")
		}
	}
}
