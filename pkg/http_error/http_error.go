// Package http_error config http errors.
package http_error

import "net/http"

var (
	serverInternalError      = "Server Internal Error"
	methodNotAllowedError    = "Method not Allowed"
	notFoundError            = "Not Found"
	unprocessableEntityError = "Unprocessable Entity"
)

// HTTPErr error interface.
type HTTPErr interface {
	GetCode() int
}

// HTTPError struct http error.
type HTTPError struct {
	Code    int         `json:"status"`
	Error   string      `json:"error"`
	Message string      `json:"message,omitempty"`
	Payload interface{} `json:"payload,omitempty"`
}

// NewInternalServerError make new 500 error.
func NewInternalServerError(message string) HTTPErr {
	return &HTTPError{
		Code:    http.StatusInternalServerError,
		Error:   serverInternalError,
		Message: message,
	}
}

// NewUnprocessableEntityError make new 422 error.
func NewUnprocessableEntityError(message string) HTTPErr {
	return &HTTPError{
		Code:    http.StatusUnprocessableEntity,
		Error:   unprocessableEntityError,
		Message: message,
	}
}

// NewMethodNotAllowedError make new 405 error.
func NewMethodNotAllowedError(message string) HTTPErr {
	return &HTTPError{
		Code:    http.StatusMethodNotAllowed,
		Error:   methodNotAllowedError,
		Message: message,
	}
}

// NewNotFoundError make new 404 error.
func NewNotFoundError(message string) HTTPErr {
	return &HTTPError{
		Code:    http.StatusNotFound,
		Error:   notFoundError,
		Message: message,
	}
}

// GetCode return error code.
func (e *HTTPError) GetCode() int {
	return e.Code
}
