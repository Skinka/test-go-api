package database

import (
	"fmt"

	"github.com/pkg/errors"
	"gorm.io/gorm"

	"gitlab.com/mi-max/api/internal/model"
)

// GetCategories return all active categories.
func (db *DB) GetCategories(lang *model.Language) ([]*model.Category, error) {
	m := make([]*model.Category, 0)
	err := db.conn.Raw(`
SELECT 
    cat.ID,
	cat.slug,
	cl.title,
	cl.description,
	cl.seo_h1,
	cl.seo_title,
	cl.seo_description,
	cl.seo_og_title,
	cl.seo_og_description 
FROM
	categories as cat 
	LEFT JOIN category_langs as cl ON cat.id = cl.parent_id AND cl.lang_id = ?
WHERE cat.is_active = TRUE 
ORDER BY cat.sort DESC
		`, lang.ID).
		Find(&m).Error
	if err != nil {
		return nil, fmt.Errorf("get categories: %w", err)
	}

	return m, nil
}

// GetCategory get category by id.
func (db *DB) GetCategory(catID uint, lang *model.Language) (*model.Category, error) {
	cat := &model.Category{}
	err := db.conn.Raw(`
SELECT 
    cat.id,
	cat.slug,
	cl.title,
	cl.description,
	cl.seo_h1,
	cl.seo_title,
	cl.seo_description,
	cl.seo_og_title,
	cl.seo_og_description 
FROM
	categories as cat 
	LEFT JOIN category_langs as cl ON cat.id = cl.parent_id AND cl.lang_id = ?
WHERE cat.is_active = true and cat.id = ?
ORDER BY cat.sort DESC
		`, lang.ID, catID).
		First(&cat).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("get category: %w", err)
	}

	return cat, nil
}
