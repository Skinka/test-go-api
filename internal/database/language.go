package database

import (
	"fmt"

	"gitlab.com/mi-max/api/internal/model"
)

// GetMainLanguage get default language.
func (db *DB) GetMainLanguage() (*model.Language, error) {
	m := &model.Language{}
	err := db.conn.
		Select("id, slug, title, code").
		Where("is_active = ?", true).
		Where("is_main = ?", true).
		First(m).Error
	if err != nil {
		return nil, fmt.Errorf("get main language: %w", err)
	}

	return m, nil
}

// GetLanguages return languages list.
func (db *DB) GetLanguages() ([]*model.Language, error) {
	m := make([]*model.Language, 0)
	err := db.conn.
		Select("id, slug, title, code").
		Where("is_active = ?", true).
		Find(&m).Error
	if err != nil {
		return nil, fmt.Errorf("get main language: %w", err)
	}

	return m, nil
}
