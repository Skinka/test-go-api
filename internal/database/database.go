// Package database слой работы с БД, содержит все методы для получения данных из БД.
// Каждый метод БД, должен принимать контекст, в котором лежит логгер.
package database

import (
	"fmt"

	"github.com/pressly/goose/v3"
	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"moul.io/zapgorm2"

	"gitlab.com/mi-max/api/pkg/config"
)

// DB initialize methods for use DB.
type DB struct {
	conf *config.DB
	log  *zap.Logger
	conn *gorm.DB
}

// New создаёт новый объект доступа к БД, со своим пулом подключений.
// Проверяет полученный коннект.
func New(conf *config.DB, logConf *config.Log, log *zap.Logger) (*DB, error) {
	gormDB, err := newConnect(conf)
	if err != nil {
		return nil, fmt.Errorf("newConnect: %w", err)
	}

	db := &DB{
		conf: conf,
		log:  log.With(zap.Bool("database", true)),
		conn: gormDB,
	}

	if err = db.Ping(); err != nil {
		return nil, fmt.Errorf("db.Ping: %w", err)
	}

	gLog := zapgorm2.New(log)
	gLog.IgnoreRecordNotFoundError = true

	if logConf.DebugDB {
		gLog.LogLevel = logger.Info
	}

	gLog.SetAsDefault()
	db.conn.Logger = gLog

	return db, nil
}

// newConnect make new pull connections to DB.
func newConnect(c *config.DB) (*gorm.DB, error) {
	connString := "host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=UTC"
	connString = fmt.Sprintf(connString, c.Host, c.User, c.Pass, c.Name, c.Port)

	db, err := gorm.Open(postgres.Open(connString), &gorm.Config{})
	if err != nil {
		return nil, fmt.Errorf("gorm.Open: %w", err)
	}

	sqlDB, err := db.DB()
	if err != nil {
		return nil, fmt.Errorf("failed to get sql connect: %w", err)
	}

	sqlDB.SetMaxOpenConns(c.MaxOpenConns)
	sqlDB.SetMaxIdleConns(c.MaxIdleConns)

	return db, nil
}

// Ping отправляет пинг, стандартными средствами БД.
func (db *DB) Ping() error {
	sqlDB, err := db.conn.DB()
	if err != nil {
		return fmt.Errorf("failed to get sql connect: %w", err)
	}

	if err = sqlDB.Ping(); err != nil {
		return fmt.Errorf("sqlDB.Ping: %w", err)
	}

	return nil
}

// RunMigrations прогоняет sql миграции на БД.
func (db *DB) RunMigrations() error {
	sqlDB, err := db.conn.DB()
	if err != nil {
		return fmt.Errorf("failed to get sql connect: %w", err)
	}

	if err = goose.SetDialect("postgres"); err != nil {
		return fmt.Errorf("goose.SetDialect: %w", err)
	}

	if err = goose.Up(sqlDB, db.conf.MigrationPath); err != nil {
		return fmt.Errorf("goose.Up: %w", err)
	}

	return nil
}
