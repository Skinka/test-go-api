// Package model package with database models.
package model

// SeoModel seo fields for another models.
type SeoModel struct {
	SeoH1            string `gorm:"seo_h1" json:"seo_h1,omitempty"`
	SeoTitle         string `gorm:"seo_title" json:"seo_title"`
	SeoDescription   string `gorm:"seo_description" json:"seo_description"`
	SeoOgTitle       string `gorm:"seo_og_title" json:"seo_og_title"`
	SeoOgDescription string `gorm:"seo_og_description" json:"seo_og_description"`
}
