// Package model package with database models.
package model

import (
	"time"
)

// Category model for "categories" table.
type Category struct {
	ID            uint   `gorm:"primarykey" json:"id,omitempty"`
	Sort          int    `gorm:"sort" json:"-"`
	IsActive      bool   `gorm:"is_active" json:"-"`
	IsHideSitemap bool   `gorm:"is_hide_sitemap" json:"-"`
	Slug          string `gorm:"slug" json:"slug"`
	Title         string `gorm:"title" json:"title"`
	Description   string `gorm:"description" json:"description,omitempty"`
	SeoModel
	CreatedAt time.Time `gorm:"created_at" json:"-"`
	UpdatedAt time.Time `gorm:"updated_at" json:"-"`
}

// TableName getting table name for gorm.
// nolint:unused // interface implementation.
func (*Category) TableName() string {
	return "categories"
}
