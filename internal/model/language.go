// Package model package with database models.
package model

import (
	"time"
)

// Language model for "languages" table.
type Language struct {
	ID        uint      `gorm:"primarykey" json:"-"`
	IsActive  bool      `gorm:"is_active" json:"-"`
	IsMain    bool      `gorm:"is_main" json:"-"`
	Title     string    `gorm:"title" json:"title"`
	Slug      string    `gorm:"slug" json:"slug"`
	Code      string    `gorm:"code" json:"code"`
	CreatedAt time.Time `gorm:"created_at" json:"-"`
	UpdatedAt time.Time `gorm:"updated_at" json:"-"`
}

// TableName getting table name for gorm.
// nolint:unused // interface implementation.
func (*Language) TableName() string {
	return "languages"
}
