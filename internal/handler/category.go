package handler

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"gitlab.com/mi-max/api/pkg/http_error"
)

// GetCategoriesList action return json active categories list.
func (h *Handler) GetCategoriesList(w http.ResponseWriter, r *http.Request) {
	lang, err := h.getLanguage(r)
	if err != nil {
		h.ResponseErrorWithLog(w, "get languages", err, http_error.NewInternalServerError(""))
		return
	}

	categories, err := h.db.GetCategories(lang)
	if err != nil {
		h.ResponseErrorWithLog(w, "get categories", err, http_error.NewInternalServerError(""))
		return
	}

	h.ResponseJSON(w, http.StatusOK, categories)
}

// GetCategory action return category.
func (h *Handler) GetCategory(w http.ResponseWriter, r *http.Request) {
	lang, err := h.getLanguage(r)
	if err != nil {
		h.ResponseErrorWithLog(w, "get languages", err, http_error.NewInternalServerError(""))
		return
	}

	id, err := strconv.ParseUint(chi.URLParam(r, "ID"), 10, 64)
	if err != nil {
		h.ResponseError(
			w,
			http_error.NewUnprocessableEntityError("Category ID Unprocessable"),
		)

		return
	}

	category, err := h.db.GetCategory(uint(id), lang)
	if err != nil {
		h.ResponseErrorWithLog(w, "get category", err, http_error.NewInternalServerError(""))
		return
	}

	if category == nil {
		h.ResponseError(w, http_error.NewNotFoundError("Category Not Found"))
		return
	}

	h.ResponseJSON(w, http.StatusOK, category)
}
