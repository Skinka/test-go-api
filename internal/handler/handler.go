// Package handler api actions.
package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/mi-max/api/internal/model"
	"gitlab.com/mi-max/api/pkg/http_error"
	"gitlab.com/mi-max/api/pkg/middleware"
)

// ErrLanguageNotFound language not found error.
var ErrLanguageNotFound = errors.New("Language not found")

type repoDB interface {
	GetLanguages() ([]*model.Language, error)
	GetMainLanguage() (*model.Language, error)
	GetCategories(lang *model.Language) ([]*model.Category, error)
	GetCategory(catID uint, lang *model.Language) (*model.Category, error)
}

// Handler actions handler.
type Handler struct {
	log       *zap.Logger
	db        repoDB
	languages map[string]*model.Language
}

// New make new handler.
func New(log *zap.Logger, db repoDB) *Handler {
	return &Handler{
		log: log,
		db:  db,
	}
}

// Init handler data init.
func (h *Handler) Init() error {
	ls, err := h.db.GetLanguages()
	if err != nil {
		return fmt.Errorf("get languages: %w", err)
	}

	h.languages = make(map[string]*model.Language, len(ls))
	for i := 0; i < len(ls); i++ {
		h.languages[ls[i].Slug] = ls[i]
	}

	return nil
}

func (h *Handler) getLanguage(r *http.Request) (*model.Language, error) {
	l, ok := r.Context().Value(middleware.LanguageKey{}).(string)
	if !ok {
		return nil, ErrLanguageNotFound
	}

	lang, ok := h.languages[l]
	if !ok {
		return nil, ErrLanguageNotFound
	}

	return lang, nil
}

// ResponseJSON show json response.
func (h *Handler) ResponseJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		h.log.Error("marshal payload", zap.Error(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	_, err = w.Write(response)
	if err != nil {
		h.log.Error("write response", zap.Error(err))
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
}

// ResponseError show json error response.
func (h *Handler) ResponseError(w http.ResponseWriter, httpErr http_error.HTTPErr) {
	h.ResponseJSON(w, httpErr.GetCode(), httpErr)
}

// ResponseErrorWithLog show json error response and write error in log.
func (h *Handler) ResponseErrorWithLog(w http.ResponseWriter, logMes string, err error, httpErr http_error.HTTPErr) {
	h.log.Error(logMes, zap.Error(err))
	h.ResponseJSON(w, httpErr.GetCode(), httpErr)
}
