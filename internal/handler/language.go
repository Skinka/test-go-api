package handler

import (
	"net/http"

	"gitlab.com/mi-max/api/pkg/http_error"
)

// GetLanguagesList return active languages list.
func (h *Handler) GetLanguagesList(w http.ResponseWriter, r *http.Request) {
	languages, err := h.db.GetLanguages()
	if err != nil {
		h.ResponseErrorWithLog(w, "get languages", err, http_error.NewInternalServerError(""))
		return
	}

	h.ResponseJSON(w, http.StatusOK, languages)
}

// GetLanguageMain return main language.
func (h *Handler) GetLanguageMain(w http.ResponseWriter, r *http.Request) {
	language, err := h.db.GetMainLanguage()
	if err != nil {
		h.ResponseErrorWithLog(w, "get main language", err, http_error.NewInternalServerError(""))
		return
	}

	h.ResponseJSON(w, http.StatusOK, language)
}
