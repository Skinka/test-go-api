// Package web_server routing.
package web_server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"

	"gitlab.com/mi-max/api/pkg/http_error"
	m "gitlab.com/mi-max/api/pkg/middleware"
)

func (s *WebServer) routerInit() (*chi.Mux, error) {
	r := chi.NewRouter()
	if err := s.baseMiddlewareInit(r); err != nil {
		return nil, fmt.Errorf("set middleware: %w", err)
	}
	s.routes(r)
	return r, nil
}

func (s *WebServer) baseMiddlewareInit(r *chi.Mux) error {
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(m.Logger(s.log))
	r.Use(middleware.Recoverer)
	r.Use(middleware.StripSlashes)
	r.Use(middleware.Timeout(time.Duration(s.conf.Timeout) * time.Second))
	defaultLang, err := s.db.GetMainLanguage()
	if err != nil {
		return fmt.Errorf("database: %w", err)
	}

	langList, err := s.db.GetLanguages()
	if err != nil {
		return fmt.Errorf("database: %w", err)
	}

	list := make([]string, len(langList))
	for i := range langList {
		list[i] = langList[i].Slug
	}

	r.Use(m.Language(list, defaultLang.Slug))
	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))
	return nil
}

func (s *WebServer) routes(r *chi.Mux) {
	r.MethodNotAllowed(func(w http.ResponseWriter, r *http.Request) {
		s.handler.ResponseError(w, http_error.NewMethodNotAllowedError(
			fmt.Sprintf("Method %q not Allowed this Route", r.Method),
		))
	})

	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		s.handler.ResponseError(w, http_error.NewNotFoundError(fmt.Sprintf("Path %q not Found", r.URL.Path)))
	})

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("hi"))
		if err != nil {
			return
		}
	})

	// get Languages list
	r.Get("/languages", s.handler.GetLanguagesList)
	r.Get("/languages/main", s.handler.GetLanguageMain)

	// get categories list
	r.Get("/categories", s.handler.GetCategoriesList)
	r.Get("/categories/{ID:\\d+}", s.handler.GetCategory)
}
