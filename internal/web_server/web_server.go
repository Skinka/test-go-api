// Package web_server web server.
package web_server

import (
	"fmt"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/mi-max/api/internal/handler"
	"gitlab.com/mi-max/api/internal/model"
	"gitlab.com/mi-max/api/pkg/config"
)

type repo interface {
	GetMainLanguage() (*model.Language, error)
	GetLanguages() ([]*model.Language, error)
}

// WebServer main struct.
type WebServer struct {
	log     *zap.Logger
	conf    *config.WebServer
	handler *handler.Handler
	db      repo
}

// New make web server instance.
func New(log *zap.Logger, conf *config.WebServer, handler *handler.Handler, db repo) *WebServer {
	return &WebServer{
		log:     log.With(zap.Bool("web_server", true)),
		conf:    conf,
		handler: handler,
		db:      db,
	}
}

// Run start listen.
func (s *WebServer) Run() {
	r, err := s.routerInit()
	if err != nil {
		s.log.Error("router init", zap.Error(err))
	}

	err = http.ListenAndServe(fmt.Sprintf("%s:%s", "", s.conf.Port), r)
	if err != nil {
		s.log.Error("start", zap.Error(err))
	}
}
